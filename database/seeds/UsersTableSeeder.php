<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Long Dương',
                'email' => 'huulongduong@gmail.com',
                'code' => 'KD0001',
                'password' => bcrypt('Longduong'),
                'address' => '232/7 Lý Thường Kiệt, P.14, Q.10',
                'work_number' => '0988583457',
                'personal_number' => '0988583457',
                'image_path' => '',
                'remember_token' => null,
                'created_at' => '2016-06-04 13:42:19',
                'updated_at' => '2016-06-04 13:42:19',
            )
        ));
    }
}
